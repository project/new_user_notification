# New User Notification

## Introduction

This module allows multiple registered users with the "Administer Users"
permission to be notified by email when a new user needs approval.

By default, Drupal core allows a single email address to be notified when a
new user registers and requires approval. However, this is not optimal for
sites having multiple administrators, because only one email address can
receive the notification. This module addresses the problem by allowing an
administrator to select additional email addresses, from any account with the
"Administer Users" permission, to receive the "Admin (user awaiting approval)"
email.

- For a full description of this project, visit the project page: https://www.drupal.org/project/new_user_notification
- To submit bug reports and feature suggestions, or track changes: https://www.drupal.org/project/issues/new_user_notification

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Configuration

(to be added)

## Maintainers

Current maintainers:
- Paul McKibben (paulmckibben) - https://www.drupal.org/u/paulmckibben

Sponsored by:
- Turbojet Technologies - https://www.turbojettech.com
